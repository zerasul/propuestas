---
layout: 2020/post
section: proposals
category: devrooms
title: Tecnología para el fomento de la lengua andaluza
---

En [AndaluGeeks](https://andaluh.es) nos gustaría contar con un espacio dentro del evento eslibre. Debido a la carga política en cuanto a derechos digitales que hay detrás del evento, nos sentimos muy ilusionados de poder crear comunidad en torno al evento.

## Comunidad o grupo que lo propone

[AndaluGeeks](https://andaluh.es) somos un grupo de profesionales de la informática, la programación, el diseño gráfico y las TIC que creamos, desarrollamos y gestionamos proyectos tecnológicos de codigo libre y abierto alrededor de la lengua, la educación y la difusión de la cultura andaluza.

Tras dos años de recorrido, **AndaluGeeks lo componemos 40 personas**, 12 de las cuales contribuyen con código a nuestras librerías de desarrollo y aplicaciones. Visita [nuestro espacio en GitHub](https://github.com/andalugeeks) para conocer más. Nuestras apps más significativas son:

-   **Andaluh SDK**: Una librería de transcripción castellano a andaluz escrito. Disponible para [python](https://github.com/andalugeeks/andaluh-py), [javascript](https://github.com/andalugeeks/andaluh-js), [java](https://github.com/andalugeeks/andaluh-java), [.NET](https://github.com/andalugeeks/andaluh-net), [rust](https://github.com/andalugeeks/andaluh-rs) y [API REST](https://api.andaluh.es/docs)
-   **Transcriptor Andaluh**: Una aplicación [web](https://andaluh.es/transcriptor/) y [móvil](https://play.google.com/store/apps/details?id=es.andaluh.transcriptor&showAllReviews=true) para transcribir castellano a andaluz basada en [andaluh-cordova](https://github.com/andalugeeks/andaluh-cordova) (phonegap).
-   **Teclado Andaluh**: Un [teclado #opensource para escribir diréctamente en ortografía andaluza](https://andaluh.es/teclado-andaluz), con autocorrecciones, sugerencias y texto predictivo. [Disponible para Android](https://play.google.com/store/apps/details?id=com.anysoftkeyboard.languagepack.andalusia).
-   **Bots para apps de mensajería**: Disponible para [Telegram](https://github.com/andalugeeks/andaluh-telegram), [Slack](https://github.com/andalugeeks/andaluh-slack) y [Discord](https://github.com/andalugeeks/andaluh-discord).
-   **Minecraft Andaluz**: Mojang, la empresa detrás de Minecraft, aceptó [nuestra traducción a Andaluz](https://andaluh.es/minecraft-andaluz) y hoy está disponible de forma oficial.

Como comunidad abierta de desarrolladoras andaluzas queremos contribuir en nuestra tierra al establecimiento y la potenciación de redes profesionales en el campo tecnológico. Si te quieres unir a nuestra comunidad: [¡te esperamos en slack!](https://join.slack.com/t/andalugeeks/shared_invite/enQtNjE1NjQyNjU0ODg2LTQxYmYzYWM5MWI1YWMwMzBjMGM0NmQ0MDk2YjU0YzdjNzc1YWM4YTNjMmZlNGJjYTYyY2I4NmJlYzMzNzg3MTA)

### Contactos

-   Sergio Soto Núñez: sergio.soto.nunez at gmail dot com
-   J. Félix Ontañón: felixonta at gmail dot com

## Público objetivo

Se trata de un espacio abierto a todas las personas (técnicas o no) que sientan curiosidad por el andaluz escrito. Muy especialmente si tienen interés en los siguientes temas:

-   **La internacionalización (i18n) y localización (i10n) de aplicaciones**. Es algo que muchas veces damos por obvio. El ejercicio en AndaluGeeks muestra las dificultades y retos a la hora de incorporar un nuevo item de i18n e i10n.
-   **Inteligencia Artificial**. Hemos comenzado a desarrollar la transcripción inversa andaluh a castellano y vamos a utilizar técnicas avanzadas de machine-learning y redes neuronales para ello.
-   **DevOps**. En AndaluGeeks autogestionamos nuestra infraestructura de servicios con docker. Queremos incorporar mecanismos de integración continua / despliegue continuo (CICD) y aplicar una capa de API Management a nuestro [API REST](https://api.andaluh.es/docs).

## Tiempo

Medio día.

## Día

Indiferente.

## Formato

La actividad estará organizada en varios tracks, todos consecutivos:

* 11.00 - 12.15 AndaluGeeks: impulsando el andaluz escrito con software libre
    * Descripción: Presentación de bienvenida, en formato mesa redonda donde un moderador turnará distintos ponentes para presentar el trabajo realizado y proyectos en curso de Andalugeeks. El objetivo es captar usuarios y colaboradores para nuestras herramientas e invitarlos a las sesiones que se realizarán a continuación.
    * Público Objetivo: Personas no-técnicas que quieran conocer de las herramientas libres de Andalugeeks. Personas técnicas que quieran conocer las herramientas y quieran contribuir con nuestra comunidad.
    * Contenidos y Ponentes:
        * 11.00 - 11.20 Apps actuales: transcriptor y teclado (Félix Ontañón)
        * 11.20 - 11.35 Wikipedia n’Andalûh (Fernando Gallego)
        * 11.35 - 11:50 Andaluh.NET: caso de desarrollo Andalûh SDK (J.M. Sanfer)
        * 11.50 - 12.05 De andalûh a castellano con Machine Learning (Rafa Haro)
        * 12:05 - 12.15 Futuros proyectos (Félix Ontañón)
    * Presentador/Moderador: Sergio Soto.


* 12:30 - 13.45 AndaluGeeks Roadmap 2020-2021
    * Descripción: Reunión de la comunidad de desarrolladores de AndaluGeeks para discutir sobre el roadmap 2020-2021 de la comunidad.
    * Público Objetivo: Contributors actuales de AndaluGeeks, pero sobre todo  newcomers que se quieran incorporar a alguno de nuestros proyectos actuales o futuros. Si sabes de python, java, dotnet, php, javascript, android, ios, machine learning o realidad aumentada y quieres contribuir con AndaluGeeks pero no sabes cómo .. ¡apúntate y lo comentamos!
    * Contenidos:
        * 12.30 - 13.45 Estaremos todos reunidos e iremos hablando de ….
            * Andalûh SDK: transcripción multi-lenguaje.
            * Andalûh ML: transcripción inversa y realidad aumentada.
            * And-DevOps: mantenimiento de servidores de la comunidad.


* 17:30 - 18.45 Desarrollo open source de una ortografía. El caso único del Andalûh.
    * Descripción: Presentación en formato master class (charla). Se presentará el Andalûh EPA como caso paradigmático de desarrollo en abierto de una propuesta ortográfica, licenciada con Creative Commons. El objetivo es resaltar esta experiencia como re-aprovechable para el desarrollo de otras iniciativas similares más allá del Andaluz.
    * Público Objetivo: Personas tanto técnicas como no técnicas que quieran saber cómo funciona Andalûh EPA y cómo se puede aprovechar esta experiencia y “hacerle un fork”.
    * Contenidos y Ponentes:
        * 17.30 - 18.00 Andalûh EPA: ortografía desarrollada colaborativamente, en red. (Nacho Arriate)
        * 18.00 - 18.30 Implementación de un transcriptor castellano-andaluz (Nono Feui)
        * 18:30 - 18.45 ISO-639 para impulsar la i18n e i10n. La siguiente frontera (Nono Feui)

## Comentarios

## Condiciones

-   [x]  Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a los asistentes y ponentes esta aceptación.
-   [x]  Al menos una persona entre los que proponen la devroom estará presente el día agendado para la _devroom_.
-   [x]  Acepto coordinarme con la organización de esLibre.
-   [x]  Entiendo que si no hay un programa terminado para la fecha que establezca la organización, la _devroom_ podría retirarse.
