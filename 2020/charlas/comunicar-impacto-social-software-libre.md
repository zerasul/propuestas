---
layout: 2020/post
section: proposals
category: talks
title: Comunicar el impacto social de la industria del software libre
---

Descripción del concepto de responsabilidad social corporativa aplicado a las empresas contribuyentes al software libre y potenciales ventajas para mejorar el reconocimiento institucional del mismo. Siendo el contenido  ajeno al mundo tecnológico e introductorio, se plantea la charla en formato relámpago. El objetivo es que tras la charla los asistentes sean capaces de comprender el concepto de responsabilidad social corporativa y las potenciales ventajas derivadas de su comunicación en informes empresariales.  

## Formato de la propuesta

Indicar uno de estos:

-   [ ]  Charla (25 minutos)
-   [x]  Charla relámpago (10 minutos)

## Descripción

El software libre tiene un déficit de reconocimiento institucional. En los informes corporativos es aún mayor, pues lo que no se puede ni medir ni reportar no existe. Reducir este déficit informativo puede mejorar la percepción del impacto social del software libre e incluso crear nuevas fuentes de financiación para la actividad a través de los inversores socialmente responsables.

Por ello, en la charla se propone emplear para las contribuciones empresariales de software libre el marco de comunicación de actividades socialmente responsables, usualmente aplicado a actuaciones medioambientales (p.e. plantar árboles, sanear ecosistemas, reciclar residuos, etc) o sobre comunidades desfavorecidas (p.e apoyar programas de integración social, financiar escuelas en países en desarrollo, etc). En las empresas que desarrollan software libre este impacto social es consecuencia de su propia actividad, e intrínsecamente inseparable de ella, pues liberar código o aportar medios para su desarrollo es una contribución activa y voluntaria a la  mejora social y económica de la sociedad aún teniendo el objetivo final de mejorar la posición competitiva y el valor añadido de la empresa. Sin embargo, fuera de la comunidad del software libre, esto es absolutamente desconocido.

## Público objetivo

Responsables de empresas que desarrollan software o cultura libre.

## Ponente(s)

**Jesús García García**, soy profesor del departamento de Contabilidad de la universidad de Oviedo y mis líneas de investigación se han centrado en la interacción entre personas, tecnología digital y rendición de cuentas, y en los aspectos económicos y financieros del software libre.

-   Web: [www.jesusgarcia.info](http://jesusgarcia.info/)
-   Twitter: [@jesgar](https://twitter.com/jesgar)

### Contacto(s)

-   **Jesús García García**: jesgar at uniovi dot es

## Comentarios

Según las necesidades de la organización podría plantearse la charla en formato normal y no relámpago.

## Condiciones

-   [x]  Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a los asistentes y ponentes esta aceptación.
-   [x]  Al menos una persona entre los que la proponen estará presente el día programado para la charla.
