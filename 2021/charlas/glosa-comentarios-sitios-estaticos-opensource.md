---
layout: 2021/post
section: proposals
category: talks
author: Andros Fenollosa Hurtado
title: Glosa, comentarios para sitios estáticos. Un clone de Disqus Opensource y sexy.
---

Glosa es una solución Opensource que intenta acabar con la gran dependencia que existe sobre algunos servicios como Disqus y otros sistemas que incorporan comentarios a sitios estáticos haciéndote dueño de tu información.

## Formato de la propuesta

Indicar uno de estos:
-   [ ]  Charla corta (10 minutos)
-   [x]  Charla (25 minutos)

## Descripción

Glosa es una solución Opensource que intenta acabar con la gran dependencia que existe sobre algunos servicios como Disqus y otros sistemas que incorporan comentarios a sitios estáticos. Ofreciendo todo lo necesario para independizarse a nivel tecnológico: API, integración en HTML, PWA para su administración e importador.

Está construido sobre Clojure usando Tadam Web Framework y Vue para el Front-End, además de mucha pasión y amor por el software libre.

-   Web del proyecto: <https://github.com/glosa/>

## Público objetivo

- Blogueros.
- Desarrolladores Web.
- Quienes busquen ser dueños de su información.
- DevOps.

## Ponente(s)

Es desarrollador Fullstack, Project Manager en Sapps estudio. Compagina su actividad profesional con la docencia; siendo su mayor pasión. Ha dado otras charlas y talleres como la PyConES y WordCamp. Colabora en el Podcast “República Web”, bloguero, enamorado de la programación funcional y donante de código en diversos proyectos Opensource.

### Contacto(s)

-   Nombre: Andros Fenollosa Hurtado
-   Email: andros@fenollosa.email
-   Web personal: <https://programadorwebvalencia.com/>
-   Mastodon (u otras redes sociales libres): <https://mastodon.technology/web/accounts/187024>
-   Twitter: <https://twitter.com/androsfenollosa>
-   GitLab: <https://gitlab.com/tanrax>
-   Portfolio o GitHub (u otros sitios de código colaborativo): <https://github.com/tanrax/>

## Comentarios



## Preferencias de privacidad

-   [x]  Doy permiso para que mi email de contacto sea publicado con la información de la charla.
-   [x]  Doy permiso para que mis redes sociales sean publicadas con la información de la charla.

## Condiciones aceptadas

-   [x]  Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a las personas asistentes su cumplimiento.
-   [x]  Confirmo que al menos una persona de entre las que proponen la charla estará conectada el día programado para exponerla.
