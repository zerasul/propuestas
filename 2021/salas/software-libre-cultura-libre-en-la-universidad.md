---
layout: 2021/post
section: proposals
category: devrooms
title: Software Libre y Cultura Libre en la universidad
---

## Descripción de la sala

El objetivo de esta propuesta es discutir el estado actual del software libre en las universidades españolas y como se podría reconducir el mismo para volver a revitalizarlo. Después de una época que comenzó hace unos 15 años con muchas universidades animándose a apoyar "eso del software libre", pocas siguen operativas hoy en día: algunas porque nunca llegó a existir un interés real por parte de las propias universidades más allá de aparecer en clasificaciones como el RuSl ([Ranking de universidades en Software Libre](https://www.portalprogramas.com/software-libre/ranking-universidades/), otras porque perdieron el tibio apoyo de un determinado equipo de gobierno con la llegada de nuevas elecciones rectorales...

El escenario puede parecer poco esperanzador, sin embargo, nunca han dejado de existir iniciativas que se resisten a rendirse ante la defensa de la importancia y la necesidad de las tecnologías y la cultura libres en la educación y la ciencia.

Precisamente por eso queremos crear un espacio donde podamos comentar los problemas actuales, así como trazar posibles líneas de actuación que incluyeran a todos los colectivos que representan la comunidad universitarias: estudiantes, personal y profesores.

## Comunidad que la propone

#### Oficinas y asociaciones universitarias de software libre

Toda persona que tenga ideas, iniciativas, proyectos o experiencias en este ámbito es bienvenida, pero para ir poniendo ideas en común, nos estamos agrupando en grupo de personas que nos podrán comentar sus experiencias personales desde:

- Oficina de Conocimiento y Cultura Libres de la Universidad Rey Juan Carlos
- Oficina de Software Libre de la Universidad de Granada
- Oficina de Software Libre de la Universidad de La Laguna
- Oficina de Software Libre de la Universidad de Zaragoza
- Oficina de Software Libre y Tecnologías Abiertas de la Universidad Complutense de Madrid
- Aula de Software Libre de la Universidad de Córdoba
- LibreLabUCM (Universidad Complutense de Madrid)
- SUGUS GNU/Linux (Universidad de Sevilla)
- Concurso Universitario de Software Libre


- Universidad Complutense de Madrid
- Universidad de Cádiz
- Universidad de Castilla La Mancha
- Universidad de Córdoba
- Universidad de Granada
- Universidad de La Laguna
- Universidad de Las Palmas de Gran Canaria
- Universidad de Oviedo
- Universidad de Sevilla
- Universidad de Zaragoza
- Universidade da Coruña
- Universidade de Santiago de Compostela
- Universidade de Vigo
- Universidad Rey Juan Carlos
- Universitat Autònoma de Barcelona


Grupos que apoyan activamente esta iniciativa:
- LibreLabGRX
- Interferencias

-   Web de la comunidad: <https://eslib.re/>
-   Mastodon (u otras redes sociales libres): <https://floss.social/@eslibre/>
-   Twitter: <https://twitter.com/esLibre_>
-   GitLab: <https://gitlab.com/eslibre>
-   Portfolio o GitHub (u otros sitios de código colaborativo):

### Contacto(s)

-   Nombre de contacto: Germán Martínez
-   Email de contacto: germaaan@interferencias.tech

## Público objetivo

Cualquier persona que tenga la inquietud porque se fomente el software libre en la educación en general.

## Formato

El formato dependerá del número de personas que finalmente tengan intención en participar.

## Preferencia horaria

-   Duración: Por determinar en función de las personas contactadas.
-   Día: También por determinar dependiendo de las personas que participen finalmente.

## Comentarios

La intención es conocer también la realidad en las universidades de las distintas regiones, ya que así quizás podríamos ver la mejor forma de colaborar entre unas y otras.

Si perteneces a alguna universidad que no está incluida o tienes algún contacto que pudiera participar, nos harías un gran favor si nos escribes para ponernos en contacto.

Gracias!

## Preferencias de privacidad

-   [x]  Damos permiso para que nuestro email de contacto sea publicado con la información de la sala.

## Condiciones aceptadas

-   [x]  Aceptamos seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a las personas asistentes su cumplimiento.
-   [x]  Aceptamos coordinarnos con la organización de esLibre para la organización de la sala.
-   [x]  Aceptamos que la sala puede ser retirada si no hay un programa terminado para la fecha decidida por la organización.
-   [x]  Confirmamos que al menos una persona de entre las que proponen la sala estará conectada el día programado para realizarla.
