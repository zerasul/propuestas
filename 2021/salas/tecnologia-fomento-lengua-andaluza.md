---
layout: 2021/post
section: proposals
category: devrooms
community: Andalugeeks
title: Tecnología para el fomento de la lengua andaluza
---

## Descripción de la sala

En esta edición de eslibre, la comunidad de andalugeeks volvemos a proponer organizar una sala. Será un espacio enfocado tanto para las personas que pertenecen actualmente a la comunidad como a personas que se interesen por nuestros proyectos.

## Comunidad que la propone

#### Andalugeeks

AndaluGeeks somos un grupo de profesionales de la informática, la programación, el diseño gráfico y las TIC que creamos, desarrollamos y gestionamos proyectos tecnológicos de codigo libre y abierto alrededor de la lengua, la educación y la difusión de la cultura andaluza.

-   Web de la comunidad: <https://andaluh.es>
-   Mastodon (u otras redes sociales libres):
-   Twitter: <https://twitter.com/andalugeeks>
-   GitLab:
-   Portfolio o GitHub (u otros sitios de código colaborativo): https://github.com/andalugeeks/

### Contacto(s)

-   Nombre de contacto:
-   Email de contacto:

## Público objetivo

Se trata de un espacio abierto a todas las personas (técnicas o no) que sientan curiosidad por el andaluz escrito. De especial interés en las temáticas:
internacionalización (i18n) y localización (i10n) de aplicaciones, inteligencia artificial aplicada al desarrollo  de herramientas para la transcripción inversa y devops  para la gestión de pipelinse de CI/CD e infraestructura basada en docker.

## Formato

Estamos organizando charlas centradas tanto en el uso de las herramientas desarrolladas como en los nuevos proyectos. Aprovecharemos para montar algún taller donde confluiremos

## Preferencia horaria

-   Duración: Un día y medio.
-   Día: Viernes tarde y sábado completo.

## Comentarios



## Preferencias de privacidad

-   [ ]  Damos permiso para que nuestro email de contacto sea publicado con la información de la sala.

## Condiciones aceptadas

-   [x]  Aceptamos seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a las personas asistentes su cumplimiento.
-   [x]  Aceptamos coordinarnos con la organización de esLibre para la organización de la sala.
-   [x]  Aceptamos que la sala puede ser retirada si no hay un programa terminado para la fecha decidida por la organización.
-   [x]  Confirmamos que al menos una persona de entre las que proponen la sala estará conectada el día programado para realizarla.
