---
layout: 2021/post
section: proposals
category: devrooms
community: Raku & Perl mongers
title: Raku & friends
---

## Descripción de la sala

[Raku](https://raku.org) is a multi-paradigm language _created for the next 100 years_. Its wealth of features and expressivity make it an ideal choice for creative programming. We consider friends most other languages, or all of them, really, but if you need to ask, we would be interested in technologies or services that could be interfaced with Raku (data stores, APIs), or programming techniques (grammars, regular expressions, functional programming, concurrent programming) that could be of interest for the Raku community, even if code samples are not written in the language. In a word, we want to attract any programmer that enjoys their job, and wants to inspire and be inspired by other people like them.

## Comunidad que la propone

#### Raku & Perl mongers

Raku and Perl are sister languages created by Larry Wall. They have in common their whippipitude and the TIMTOWDI motto: there are many ways to do it, just find your own. Raku is a younger language, out of beta since 2015, but since then it has grown to be used by a growing community of enthusiasts, and also companies

-   Web de la comunidad: <https://raku.org>
-   Mastodon (u otras redes sociales libres):
-   Twitter: [@jjmerelo](https://twitter.com/jjmerelo)
-   Gitlab:
-   Portfolio o GitHub (u otros sitios de código colaborativo): <https://github.com/Raku>

### Contacto(s)

-   Nombre de contacto: JJ Merelo
-   Email de contacto: <jjmerelo@gmail.com>

## Público objetivo

English-speaking coding enthusiast, with or without any prior experience with the language.

## Formato

We will do a Fosdem-style devroom, with long and short talks, and the call for papers will be via pull requests to a repo in GitHub. Anyone can comment, and a single approval will get the talk accepted. Additionally, for newcomers to the language, we will run a tutorial that will help everyone get acquainted.

## Preferencia horaria

-   Duración: Un día
-   Día: Primero

## Comentarios

Como la comunidad de Raku es pequeña, vamos a hacer toda la publicidad y el CFP directamente en inglés. Dado que no ha habido, realmente, ningún evento exclusivo de Raku, queremos ver qué tal sale el experimento. En principio personas de la comunidad han expresado su interés en enviar trabajos, así que hay cierta garantía de que salga adelante.

## Preferencias de privacidad

-   [x]  Damos permiso para que nuestro email de contacto sea publicado con la información de la sala.

## Condiciones aceptadas

-   [x]  Aceptamos seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a las personas asistentes su cumplimiento.
-   [x]  Aceptamos coordinarnos con la organización de esLibre para la organización de la sala.
-   [x]  Aceptamos que la sala puede ser retirada si no hay un programa terminado para la fecha decidida por la organización.
-   [x]  Confirmamos que al menos una persona de entre las que proponen la sala estará conectada el día programado para realizarla.
