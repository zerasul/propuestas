---
layout: 2021/post
section: proposals
category: workshops
author: Juan Félix Mateos Barrado
title: myIoT&#58 Integración bidreccional de dispositivos LoRaWAN con The Things Network
---

## Descripción

myIoT ([véase charla propuesta](/2021/charlas/myiot-plataforma-internet-de-las-cosas-codigo-libre-procomun)) es una plataforma de Internet de las cosas de código abierto, desarrollada, mantenida y gestionada por sus propios usuarios en modalidad de procomún.

En este taller explicaremos cómo integrar en la plataforma un dispositivo IoT desarrollado por la comunidad The Things Network Madrid.

The Things Network es una iniciativa global para desplegar una red de Internet de las Cosas abierta, libre, neutral, segura y útil.

## Objetivos a cubrir en el taller

Configurar un dispositivos IoT en la red The Things Network.

Integrar un dispositivo de The Things Network en la plataforma myIoT.

Recibir telemetrías en myIoT, configurar alarmas y notificaciones.

Enviar comandos desde myIoT para actuar sobre un dispositivo IoT.

-   Web del proyecto: <https://my.iotopentech.io>
-   Repositorio del proyecto: <https://github.com/IoTopenTech/myIoTopenTech>

## Público objetivo

Público general

Desarrolladores de hardware IoT

## Ponente(s)

Juan Félix Mateos Barrado

Miembro de la comunidad The Things Network Madrid.

Profesor de nuevas tecnologías.

Desarrollador de prototipos electrónicos.

### Contacto(s)

-   Nombre: Juan Felix Mateos
-   Email: <juanfelixmateos@gmail.com>
-   Web personal:
-   Mastodon (u otras redes sociales libres):
-   Twitter: [@juanfelixmateos](https://twitter.com/juanfelixmateos)
-   Gitlab:
-   Portfolio o GitHub (u otros sitios de código colaborativo): <https://github.com/IoTopenTech/myIoTopenTech>

## Prerrequisitos para los asistentes

Disponer de una cuenta de correo electrónico accesible que utilizaremos para acreditarnos en The Things Network y myIoT.

## Prerrequisitos para la organización



## Preferencia horaria

-   Duración:
-   Día:

## Comentarios



## Preferencias de privacidad

-   [x]  Doy permiso para que mi email de contacto sea publicado con la información del taller.
-   [x]  Doy permiso para que mis redes sociales sean publicadas con la información del taller.

## Condiciones aceptadas

-   [x]  Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a las personas asistentes su cumplimiento.
-   [x]  Acepto coordinarme con la organización de esLibre para la realización del taller.
-   [x]  Confirmo que al menos una persona de entre las que proponen el taller estará conectada el día programado para impartirlo.
