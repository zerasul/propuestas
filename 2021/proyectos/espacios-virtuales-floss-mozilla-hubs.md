---
layout: 2021/post
section: proposals
category: projects
title: Espacios Virtuales FLOSS - Mozilla HUBS
---

En el Grupo  AvFloss venimos utilizando la plataforma Mozilla HUBS para reuniones de trabajo, realizar talleres, etc...
Es una plataforma accesible tanto desde móviles, pcs o Gafas Virtuales. que permite la creación e investigación en un entorno (VR) que usualmente es privativo y elitista.

Estamos editando una serie de  video-tutoriales en español, sobre su uso, administración  y creación de espacios XR, que estarán disponibles para las fechas del congreso esLibre.

~ ~ ~ 

-   Web del proyecto: <https://github.com/mozilla/hubs>

### Contacto(s)

-   Nombre: Avfloss
-   Email:
-   Web personal: <https://avfloss.github.io/>
-   Mastodon (u otras redes sociales libres): <https://floss.social/@av>
-   Twitter: <https://twitter.com/AV_floss>
-   GitLab:
-   Portfolio o GitHub (u otros sitios de código colaborativo): <https://github.com/AVFLOSS/avfloss.github.io>

## Comentarios



## Preferencias de privacidad

-   [ ]  Doy permiso para que mi email de contacto sea publicado con la información del proyecto.
-   [x]  Doy permiso para que mis redes sociales sean publicadas con la información del proyecto.

## Condiciones aceptadas

-   [x]  Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a las personas asistentes su cumplimiento.

